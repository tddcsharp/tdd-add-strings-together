﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TDD_AddStringsTogether
{
    [TestClass]
    public class StringConcatenatorTest
    {
        [TestMethod]
        public void test_add_two_empty_strings_result_single_space()
        {
            // Arrange
            StringConcatenator cut = new StringConcatenator();
            String lh = "";
            String rh = "";
            String expResult = " ";

            // Act
            String result = cut.addStrings(lh, rh);

            // Assert
            Assert.AreEqual(expResult, result);
        }

        [TestMethod]
        public void test_add_two_strings_result_lh_space_rh()
        {
            // Arrange
            StringConcatenator cut = new StringConcatenator();
            String lh = "AA";
            String rh = "BB";
            String expResult = "AA BB";

            // Act
            String result = cut.addStrings(lh, rh);

            // Assert
            Assert.AreEqual(expResult, result);
        }

        [TestMethod]
        public void test_add_space_and_empty_result_2x_space()
        {
            // Arrange
            StringConcatenator cut = new StringConcatenator();
            String lh = " ";
            String rh = "";
            String expResult = "  ";

            // Act
            String result = cut.addStrings(lh, rh);

            // Assert
            Assert.AreEqual(expResult, result);
        }

        [TestMethod]
        public void test_add_empty_and_space_result_2x_space()
        {
            // Arrange
            StringConcatenator cut = new StringConcatenator();
            String lh = "";
            String rh = " ";
            String expResult = "  ";

            // Act
            String result = cut.addStrings(lh, rh);

            // Assert
            Assert.AreEqual(expResult, result);
        }

        [TestMethod]
        public void test_add_strings_remove_leading_space_result_lh_space_rh()
        {
            // Arrange
            StringConcatenator cut = new StringConcatenator();
            String lh = " AA";
            String rh = "BB";
            String expResult = "AA BB";

            // Act
            String result = cut.addStrings(lh, rh);

            // Assert
            Assert.AreEqual(expResult, result);
        }

        [TestMethod]
        public void test_add_strings_lh_and_trim_rh_result_lh_space_rh()
        {
            // Arrange
            StringConcatenator cut = new StringConcatenator();
            String lh = "AA";
            String rh = " BB ";
            String expResult = "AA BB";

            // Act
            String result = cut.addStrings(lh, rh);

            // Assert
            Assert.AreEqual(expResult, result);
        }
    }
}
