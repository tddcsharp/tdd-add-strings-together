﻿using System;

namespace TDD_AddStringsTogether
{
    public class StringConcatenator
    {
        public StringConcatenator()
        {
        }

        public string addStrings(string lh, string rh)
        {
            String s1 = lh;
            String s2 = rh;
            if (true)
            {
                if (lh.Length > 1)
                    s1 = lh.Trim();
                if (rh.Length > 1)
                    s2 = rh.Trim();
            }
            return $"{s1} {s2}";
        }
    }
}